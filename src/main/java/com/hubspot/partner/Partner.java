package com.hubspot.partner;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Partner {
	private String firstName;
    private String lastName;
    private String email;
    private String country;
    private List<String> availableDates;
    
    public List<String> getValidAvailableDates(){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<String> validAvailableDates = new ArrayList<String>();
		for (int d=0; d<= availableDates.size()-2; d++ ) {
			try {
				Date currentDate = simpleDateFormat.parse(availableDates.get(d));
				Date nextDate = simpleDateFormat.parse(availableDates.get(d+1));
				int diffInDays = (int) ((currentDate.getTime() - nextDate.getTime()) / (1000 * 60 * 60 * 24));
				
	            if(diffInDays == -1){
	            	validAvailableDates.add(simpleDateFormat.format(currentDate));
	            }
			} catch (ParseException e) {
				e.printStackTrace();
			}
        }
		return validAvailableDates;
    }
    public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<String> getAvailableDates() {
		return availableDates;
	}
	public void setAvailableDates(List<String> availableDates) {
		this.availableDates = availableDates;
	}
    public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
