package com.hubspot.partner;

import java.util.List;


public class Country {
	private int attendeeCount;
    private String name;
    private String startDate;
    private List<String> attendees;
    
    public Country(String countryName, String eventStartDate, List<String> attendees){
    	this.attendeeCount = attendees.size();
        this.attendees = attendees;
        this.name = countryName;
        this.startDate = eventStartDate;
    }

	public int getAttendeeCount() {
		return attendeeCount;
	}

	public void setAttendeeCount(int attendeeCount) {
		this.attendeeCount = attendeeCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public List<String> getAttendees() {
		return attendees;
	}

	public void setAttendees(List<String> attendees) {
		this.attendees = attendees;
	}
}