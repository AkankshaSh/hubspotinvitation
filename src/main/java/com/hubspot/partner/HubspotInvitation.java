package com.hubspot.partner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import com.google.gson.Gson;

import com.hubspot.partner.Partner;
import com.hubspot.partner.Country;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HubspotInvitation {
	private static final MediaType JSON= MediaType.parse("application/json; charset=utf-8");
    private static final String GET_URL = "https://candidate.hubteam.com/candidateTest/v2/partners?userKey=5678eb3a87827d7bc021ee7392f7";
    private static final String POST_URL = "https://candidate.hubteam.com/candidateTest/v2/results?userKey=5678eb3a87827d7bc021ee7392f7";

	public static void main(String args[]){
		try{
			OkHttpClient client = new OkHttpClient();
			Request request = new Request.Builder().url(GET_URL).build();
			Response getResponse = client.newCall(request).execute();
			Gson gson = new Gson();
			String response = getResponse.body().string();
			int indexOfLastBracket = response.lastIndexOf("}");
			String subResponse = response.substring(12, indexOfLastBracket);
			Partner partners[] = gson.fromJson(subResponse, Partner[].class);
			ArrayList<Partner> partnerList = new ArrayList<Partner>(Arrays.asList(partners));
			Map<String, List<Partner>> countryMap= partnerList.stream().collect(Collectors.groupingBy(Partner::getCountry));
			Map<String, HashMap<String, List<String>>> dateCountryPersonMap = countryMap.
                    entrySet().stream().
                    collect(Collectors.toMap(e -> e.getKey(),
                            e -> getSpecialDate(e.getValue())));
			String jsonOutput=getJSON(dateCountryPersonMap);
			
        	RequestBody body = RequestBody.create(JSON, jsonOutput);
    		Request postRequest = new Request.Builder().url(POST_URL).post(body)
    	      .addHeader("content-type", "application/json; charset=utf-8").build();
    		Response postResponse = client.newCall(postRequest).execute();
    		String result =  postResponse.body().string();
    		System.out.println(result);
		}catch(Exception ex){
			System.out.println("Something went wrong!");
		}
	}
	
	private static String getJSON(Map<String, HashMap<String, List<String>>> dateCountryPersonMap) {
		Map<String, ArrayList<Country>> map = new HashMap<String, ArrayList<Country>>();
		ArrayList<Country> arr = new ArrayList<Country>();
		dateCountryPersonMap.forEach((String k, HashMap<String, List<String>> v) ->{
			v.forEach((key,val)->{
				arr.add(new Country(k, key, val));
			});
		});
		map.put("countries", arr);
		Gson gson = new Gson();
		return gson.toJson(map);
	}

	public static HashMap<String, List<String>> getSpecialDate(List<Partner> partnerList){
        Map<String, ArrayList<String>> dateEmails = new HashMap<String, ArrayList<String>>();
        partnerList.forEach(k-> {
        	List<String>validDates = k.getValidAvailableDates();
            validDates.forEach(d -> {
            	ArrayList<String> tempList = null;
            	if(dateEmails.containsKey(d)){
            		tempList = dateEmails.get(d);
            	}else{
            		tempList = new ArrayList<String>();
            	}
            	tempList.add(k.getEmail());
            	dateEmails.put(d, tempList);
            });
        });
        Map.Entry<String, ArrayList<String>> max = null;
        try{
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            for (Map.Entry<String, ArrayList<String>> e : dateEmails.entrySet()) {
                if ((max == null || e.getValue().size() > max.getValue().size()) || ((e.getValue().size() == max.getValue().size()) && (simpleDateFormat.parse(e.getKey()).compareTo(simpleDateFormat.parse(max.getKey())) < 0)))
                    max = e;
            }
        }catch(Exception e){
        	
        }
        Map<String, List<String>> output= new HashMap<String, List<String>>();
        output.put(max.getKey(), max.getValue());
        return (HashMap<String, List<String>>) output;
    }
}
