Objective : Implementation of invitation logic to get maximum attendees for an event organised for the Hubspot partners.

Implementation Details : HubspotInvitation.java contains main function. For Https calls, have used Okhttp and for JSON operations, have used Gson. External libraries are placed under libs directory. 

Execute : Build the Gradle and run as Java Application.